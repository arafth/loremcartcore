﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LoremCartCore.Models.ViewModels
{
    public class CategoryVM
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "Please input category name!")]
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsValid { get; set; }
        public string Message { get; set; }
        //public bool ClientValidationEnabled { get; }
    }
}
