﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace LoremCartCore.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class AdministrativeController : Controller
    {
        public IActionResult Dashboard()
        {
            return View();
        }
    }
}