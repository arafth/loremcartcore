﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LoremCartCore.Data;
using LoremCartCore.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;
using LoremCartCore.Models.Category;

namespace LoremCartCore.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class CategoryController : Controller
    {
        private readonly ApplicationDbContext _db;

        public CategoryController(ApplicationDbContext db)
        {
            _db = db;
        }

        [HttpGet]
        public IActionResult Index()
        {
            //return View(await _db.Category.ToListAsync());
            return PartialView("_Categories");
        }

        [HttpGet]
        public IActionResult Create()
        {
            CategoryVM categoryVm = new CategoryVM();
            return PartialView("_CreateCategory", categoryVm);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public IActionResult Create(CategoryVM categoryVM)
        {
            if (ModelState.IsValid)
            {
                Category category = new Category()
                {
                    Name = categoryVM.Name,
                    Description = categoryVM.Description,
                };
                _db.Category.Add(category);
                bool isUpdated = _db.SaveChanges() > 0;
                if(isUpdated)
                {
                    categoryVM.IsValid = true;
                    categoryVM.Message = "Category added successfully!";

                    return Json(categoryVM);
                }
                categoryVM.IsValid = false;
                categoryVM.Message = "Category Can't be created. Something went wrong. Please try again!";
                return Json(categoryVM);
            }

            categoryVM.IsValid = false;
            categoryVM.Message = "Validation failed! Please try again with valid data.";
            return Json(categoryVM);

        }

        [HttpGet]
        public IActionResult Details(int id)
        {
            Category category = _db.Category.FirstOrDefault(c => c.Id == id);
            CategoryVM categoryVM = new CategoryVM()
            {
                Name = category.Name,
                Description = category.Description
            };

            return PartialView("_CategoryDetails", categoryVM);
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            Category category = _db.Category.FirstOrDefault(c => c.Id == id);
            CategoryVM categoryVM = new CategoryVM()
            {
                Name = category.Name,
                Description = category.Description
            };
            return PartialView("_EditCategory", categoryVM);
        }

        [HttpPost]
        public IActionResult Edit(CategoryVM categoryVM)
        {
            if (ModelState.IsValid)
            {
                Category category = _db.Category.FirstOrDefault(c => c.Id == categoryVM.Id);

                category.Name = categoryVM.Name;
                category.Description = categoryVM.Description;

                _db.Category.Update(category);
                bool isUpdated = _db.SaveChanges() > 0;
                if(isUpdated)
                {
                    categoryVM.IsValid = true;
                    categoryVM.Message = "Category updated successfully!";

                    return Json(categoryVM);
                }
                categoryVM.IsValid = false;
                categoryVM.Message = "Category Can't be updated. Something went wrong. Please try again!";

                return Json(categoryVM);
            }
            categoryVM.IsValid = false;
            categoryVM.Message = "Validation failed! Please try again with valid data.";

            return Json(categoryVM);
        }

        [HttpPost]
        public IActionResult Delete(CategoryVM categoryVM)
        {

            Category category = _db.Category.FirstOrDefault(c => c.Id == categoryVM.Id);
            _db.Category.Remove(category);
            bool isUpdated = _db.SaveChanges() > 0;
            if (isUpdated)
            {
                categoryVM.IsValid = true;
                categoryVM.Message = "Category deleted successfully!";

                return Json(categoryVM);
            }
            categoryVM.IsValid = false;
            categoryVM.Message = "Category Can't be deleted. Something went wrong. Please try again!";

            return Json(categoryVM);
            
        }

        public IActionResult LoadCategory()
        {
            var draw = Request.Form["draw"].FirstOrDefault();
            var start = Request.Form["start"].FirstOrDefault();
            var length = Request.Form["length"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDir = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int recordsTotal = 0;

            var category = _db.Category.ToList();

            var categoryList = new List<CategoryVM>();

            //Sorting    
            if (!string.IsNullOrEmpty(sortColumn) && !string.IsNullOrEmpty(sortColumnDir))
            {
                category = category.AsQueryable().OrderBy(sortColumn + " " + sortColumnDir).ToList();
            }
            else
            {
                category = category.OrderByDescending(x => x.Id).ToList();
            }

            //Search    
            if (!string.IsNullOrEmpty(searchValue))
            {
                category = category.Where(x => x.Name.Contains(searchValue)).ToList();
            }

            foreach (var item in category)
            {
                categoryList.Add(new CategoryVM
                {
                    Id = item.Id,
                    Name = item.Name,
                    //IsActive = item.IsActive,

                });
            }

            //total number of rows count     
            recordsTotal = categoryList.Count();

            //Paging     
            var data = categoryList.Skip(skip).Take(pageSize).ToList();

            //Returning Json Data    
            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });
        }
    }
}