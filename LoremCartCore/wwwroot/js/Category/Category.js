﻿
$(document).ready(function () {
    const categoryForm = "form[name='categoryForm']";

    toastr.options = {
        "closeButton": false,
        "progressBar": true,
        "positionClass": 'toast-bottom-right',
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };

    var categoryDatatable = $("#categoryDatatable").dataTable({
        "processing": true,
        "serverSide": true,
        "filter": true,
        "pageLength": 10,
        "autoWidth": false,
        "lengthMenu": [[10, 20, 30, 40, 50, -1], [10, 20, 30, 40, 50, "All"]],
        "order": [[0, "asc"]],
        "ajax": {
            "url": "/Admin/Category/LoadCategory/",
            "type": "POST",
            "data": function (data) {
            },
            "complete": function (json) {
            }
        },
        "columns": [
            { "data": "name", "name": "Name", "autowidth": true },
            //{ "data": "email", "name": "Email", "autowidth": true },
            //{ "data": "phone", "name": "Phone", "autowidth": true },
            //{ "data": "description", "name": "Description", "autowidth": true },
            //{
            //    "render": function (data, type, full, meta) {
            //        var checkedStatus = "";
            //        if (full.isActive === true) {
            //            checkedStatus = "<i  class='fas fa-check-circle text-primary'></i>";
            //        }
            //        else {
            //            checkedStatus = "<i class='fas fa-times-circle text-danger'></i>";
            //        }
            //        return `<span style="font-size: .8rem;">${checkedStatus}</span>`;

            //    }, "autowidth": true
            //},

            {
                "render": function (data, type, full, meta) {
                    return `<button style="font-size: .8rem;color:gray;" class="btn btn-sm btn-rx btn-table categorydetailsBtn" data-id="${full.id}" data-toggle="tooltip" title="Category details"><i class="fas fa-info-circle"></i></button>
                        <button style="font-size: .8rem;color:black;" class="btn btn-sm btn-rx btn-table categoryEditBtn"  data-id="${full.id}" data-toggle="tooltip" title="Update Category info!"><i class="fas fa-pencil-alt"></i></button>
                        <button style="font-size:.8rem;color:red;" class="btn btn-sm btn-rx btn-table categoryDeleteBtn"  data-id="${full.id}" data-toggle="tooltip" title="Delete category!"><i class="fa fa-trash"></i></button>`;
                }
            }
        ]
    });

    //Hide Category input fields
    $('body').on('click', '.categoryHideBtn', function () {
        $('#categoryManupulate').hide('slow');
        console.log('Category Create field hidden!');
    });

    //Category Add Section
    $('body').on("click", "#addNewCategory", function (e) {
        console.log("Clicked new Category!");
        $.ajax({
            url: "/Admin/Category/Create",
            type: "GET",
            success: function (response) {
                console.log("Success!");
                categoryDatatable.fnFilter();
                $("#categoryManupulate").show('slow');
                $("#categoryManupulate").html(response);
                var categoryValidator = $(categoryForm).validate({
                    rules: {
                        Name: {
                            required: true
                        },
                        Description: {
                            required: false
                        }
                    },
                    messages: {
                        Name: "Please input Category name."
                    },
                    submitHandler: function (form) {

                    }
                });
            }, Error: function (response) {
                console.log("Error!");
                toastr.error('Something went wrong! Please try again.', 'Error');

            }

        });
    });

    $('body').on("click", "#categoryCreateBtn", function () {
        console.log("new category btn");
        console.log($(categoryForm).serializeArray());
        if ($(categoryForm).valid()) {
            $.ajax({
                url: "/Admin/Category/Create",
                data: $(categoryForm).serializeArray(),
                type: "POST",
                success: function (response) {
                    console.log("new category Success!");
                    categoryDatatable.fnFilter();
                    $("#categoryManupulate").hide('slow');
                    if (response.isValid === true) {
                        console.log(response.Message);
                        toastr.success(response.Message, 'Success');
                    } else {
                        toastr.error(response.Message, 'Error');
                    }
                },
                error: function () {
                    toastr.error('Something went wrong. Please try again.', 'Error');
                }

            });
        } else {
            toastr.error("Please fill up the form with valid data.", 'Validation Failed!');
        }
    });

    //Category Details Section
    $('body').on('click', ".categorydetailsBtn", function (e) {
        let jsonData = {
            id: $(this).attr("data-id")
        };
        $.ajax({
            url: "/Admin/Category/Details",
            type: "GET",
            data: jsonData,
            success: function (response) {
                console.log("Details get success!");
                $("#categoryManupulate").show('slow');
                $("#categoryManupulate").html(response);
            }, Error: function (response) {
                console.log("Error!");
                toastr.error('Something went wrong! Please try again.', 'Error');

            }
        });
    });




    //Category Edit Section

    $('body').on("click", ".categoryEditBtn", function (e) {
        console.log("Clicked Category Edit button!");

        let jsonData = {
            id: $(this).attr("data-id")
        };

        $.ajax({
            url: "/Admin/Category/Edit",
            type: "GET",
            data: jsonData,
            success: function (response) {
                console.log("Edit data pull Success!");
                $("#categoryManupulate").show('slow');
                $("#categoryManupulate").html(response);
                var categoryValidator = $(categoryForm).validate({
                    rules: {
                        Name: {
                            required: true
                        },
                        Description: {
                            required: false
                        }
                    },
                    messages: {
                        Name: "Please input Category name."
                    },
                    submitHandler: function (form) {

                    }
                });
            }, Error: function (response) {
                console.log("Error!");
                toastr.error('Something went wrong! Please try again.', 'Error');

            }

        });
    });

    $('body').on("click", "#categoryUpdateBtn", function () {
        console.log("Category Update Submitted!");
        console.log($(categoryForm).serializeArray());
        if ($(categoryForm).valid()) {
            $.ajax({
                url: "/Admin/Category/Edit",
                data: $(categoryForm).serializeArray(),
                type: "POST",
                success: function (response) {
                    console.log("Category Update Successfull!");
                    categoryDatatable.fnFilter();
                    $("#categoryManupulate").hide('slow');
                    if (response.isValid === true) {
                        console.log(response.Message);
                        toastr.success(response.Message, 'Success');
                    } else {
                        toastr.error(response.Message, 'Error');
                    }
                },
                error: function () {
                    toastr.error('Something went wrong. Please try again.', 'Error');
                }

            });
        } else {
            toastr.error("Please fill up the form with valid data.", 'Validation Failed!');
        }
    });

    $('body').on("click", ".categoryDeleteBtn", function () {
        let jsonData = {
            id: $(this).attr("data-id")
        };
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this once executed!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "cancel",
            closeOnConfirm: false,
            closeOnCancel: false
        },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: "/Admin/Category/Delete",
                        data: jsonData,
                        type: "POST",
                        success: function (response) {
                            console.log("Category deleted Successfully!");
                            if (response.isValid === true) {
                                categoryDatatable.fnFilter();
                                //console.log(response.Message);
                                setTimeout(swal("Deleted!", "Category has been deleted.", "success"), 2000);
                                //toastr.success(response.Message, 'Success');
                            } else {
                                //toastr.error(response.Message, 'Error');
                                setTimeout(function () {
                                    swal(response.Message, "error");
                                }, 2000);
                            }
                        },
                        error: function () {
                            //toastr.error('Something went wrong. Please try again.', 'Error');
                            setTimeout(function () {
                                swal("Error", 'Something went wrong. Please try again.', "error");
                            }, 2000);
                        }

                    });



                    
                } else {
                    setTimeout(function () {
                        swal("Cancelled", "Category deletion cancelled!", "error");
                    }, 2000);
                    //setTimeout(swal("Cancelled", "Category deletion cancelled :)", "error"), 2000);
                }
            });
        console.log("Category Delete Submitted!");

    });

});